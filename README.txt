$id$
DESCRIPTION
-----------

Hashtags module allow users to create hashtags for their nodes using (#) sign.

REQUIREMENTS
------------

 * Taxonomy module

 FEATURES
------------ 
 * Module uses Hashtags filter which automatically will be added to Filtered HTML & Full HTML (if exists) input formats;
 * If you have old version installed - just run /update.php and pick 6001 version for Hashtags module;
 * Regular expression for parsing hashtags has been built according rules from hashtags.org

INSTALLATION
------------

 1. CREATE DIRECTORY

    Create a new directory "hashtags" in the sites/all/modules directory and
    place the entire contents of this hashtags folder in it.

 2. ENABLE THE MODULE

    Enable the module on the Modules admin page:
      Administer > Site building > Modules (admin/build/modules)

 3. CONFIGURE HASHTAGS

    Go to Hashtags configuration page: (admin/config/content/hashtags);
    Check for what content types you want Hashtags functionality to have and submit a form;
    Use (#) in Body textarea on node add/edit page to create hashtags.
    (For example: Body : "Enable the #module" will create #module hashtag.     
 
EXAMPLE
-------------
Enable 'hashtags' module; go to (admin/config/content/hashtags) -> edit vocabulary for 'Hashtags'; 
check Page content type and submit form; go to Create Page form (node/add/page). Fill up title: 
Some title, fill up Body: "create #hashtags for their #nodes" and press Save; Doing so you create two 
hashtags - #hashtags and #terms. Where nodes body will be shown (node, node/%, taxonomy/term/% etc) the 
hashtags words turn into links of concreate term. For example, "create #hashtags for their #nodes" on (node/9) 
page is shown as "create <a class="hashtag" href="taxonomy/term/5">#hashtags</a> for their 
<a href="taxonomy/term/6">#nodes</a>".
